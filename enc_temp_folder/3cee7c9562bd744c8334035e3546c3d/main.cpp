#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <span>
#include <array>
#include <random>

#include "wrapper.hpp"

void framebufferSizeCallback(GLFWwindow* window, int width, int height);
std::string readSourceCode(const std::filesystem::path& path);

void addShader(const std::filesystem::path& path, uint32_t programId, GLenum shaderType);
void processInput(GLFWwindow* window);
void setUniform1f(const std::string& name, float value, uint32_t programId);

void calcMandelbrot(RGB* data, int width, int height);
void setColor(uint8_t& red, uint8_t& green, uint8_t& blue, int x, int y);
int iterations(int x, int y);

float vertices[] = {
     1.0f,  1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,         
     1.0f, -1.0f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,
    -1.0f, -1.0f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,
    -1.0f,  1.0f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f
};

unsigned int indices[] =
{
	0, 1, 2,
	0, 2, 3
};

double centerX = 0.0f;
double centerY = 0.0f;
double zoomIn = 1.0f;

enum class Type {
    Float, Double, FP128
};

Type type = Type::Float;

#include <bitset>

int main(int argc, char* argv[])
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    double lower_bound = -4;
    double upper_bound = 4;
    std::uniform_real_distribution<double> unif(lower_bound, upper_bound);
    std::default_random_engine re;

    for (int i = 0; i < 10000; ++i)
    {
        double val1 = unif(re);
        double val2 = unif(re);
        double plusShouldBe = val1 + val2;
        double minusShouldBe = val1 - val2;
        FP128 t(val1);
        FP128 s(val2);
        double plusResult = (t + s).toDouble();
        double minusResult = (t - s).toDouble();
        if (minusResult != minusShouldBe)
        {
            auto f = 12;
            t - s;
        }
        else if (plusResult != plusShouldBe)
        {
            auto f = 12;
            t + s;
        }
    }

    GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Mandelbrot", NULL, NULL);

    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window!\n";
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed initializing GLAD\n";
    }

    glViewport(0, 0, screenWidth, screenHeight);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    const uint32_t program = glCreateProgram();

    uint32_t VAO, VBO, EBO, textureId;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    addShader(std::filesystem::current_path() / "shader.vert", program, GL_VERTEX_SHADER);
    addShader(std::filesystem::current_path() / "shader.frag", program, GL_FRAGMENT_SHADER);
    glLinkProgram(program);

    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    RGB* data = new RGB[screenWidth * screenHeight];
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, screenWidth, screenHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

    while (!glfwWindowShouldClose(window))
    {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        processInput(window);

        glUseProgram(program);
        //
        //  Calculate mandelbrot and save it into data
        if (type == Type::Double)
        {
            cudaCallMandelbrotDouble(data, screenWidth, screenHeight, zoomIn, centerX, centerY);
        }
        else
        {
            cudaCallMandelbrotFloat(data, screenWidth, screenHeight, zoomIn, centerX, centerY);
        }
        //
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, screenWidth, screenHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glBindTexture(GL_TEXTURE_2D, textureId);
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    delete[] data;
    glDeleteProgram(program);
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    glfwTerminate();
    return 0;
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

std::string readSourceCode(const std::filesystem::path& path)
{
	std::string code;
	std::ifstream file(path, std::ios::in);

	if (!file)
	{
		std::cout << "Failed to open shader file: " << path.string() << "\n";
		return std::string();
	}

	std::string line;
	while (std::getline(file, line))
		code.append(line + '\n');
	return code;
}

void addShader(const std::filesystem::path& path, uint32_t programId, GLenum shaderType)
{
    const std::string source = readSourceCode(path);
    const char* sourceCharPtr = source.c_str();
    const uint32_t shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &sourceCharPtr, NULL);
    glCompileShader(shader);
    glAttachShader(programId, shader);
    glDeleteShader(shader);
}

void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }

    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
    {
        centerY = centerY + (0.02f * zoomIn);
        if (centerY > 2.0f)
        {
            centerY = 2.0f;
        }
    }

    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
    {
        centerY = centerY - (0.02f * zoomIn);
        if (centerY < -2.0f)
        {
            centerY = -2.0f;
        }
    }

    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
    {
        centerX = centerX - (0.02f * zoomIn);
        if (centerX < -2.0f)
        {
            centerX = -2.0f;
        }
    }

    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
    {
        centerX = centerX + (zoomIn * 0.02f);
        if (centerX > 2.0f)
        {
            centerX = 2.0f;
        }
    }

    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
    {
        zoomIn = zoomIn * 1.02f;
        if (zoomIn > 1.0f)
        {
            zoomIn = 1.0f;
        }
    }

    if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
    {
        zoomIn = zoomIn * 0.98f;
        if (zoomIn == 0)
        {
            zoomIn = std::numeric_limits<float>::min();
        }
    }

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        type = Type::Float;
    }

    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        type = Type::Double;
    }
}

void setUniform1f(const std::string& name, float value, uint32_t programId)
{
    glUniform1f(glGetUniformLocation(programId, name.c_str()), value);
}

void calcMandelbrot(RGB* data, int width, int height)
{
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            RGB& current = *(data + (y * width) + x);
            setColor(current.r, current.g, current.b, x, y);
        }
    }
}

void setColor(uint8_t& red, uint8_t& green, uint8_t& blue, int x, int y)
{
    int iter = iterations(x, y);
    if (iter == maximumIteration)
    {
        red = 0;
        green = 0;
        blue = 0;
        return;
    }
    const float div = (float)iter / maximumIteration;
    const float value = (float)255 * div;
    if (iter > 10)
        auto g = 12;
    red = 0;
    green = static_cast<uint8_t>(value);
    blue = 0;
}

int iterations(int x, int y)
{
    float real = ((x - (screenWidth / 2) + (centerX / zoomIn * screenWidth)) * zoomIn / (float)screenWidth) * 4;
    float imag = ((y - (screenHeight / 2) + (centerY / zoomIn * screenHeight)) * zoomIn / (float)screenHeight) * 4;

    int iterations = 0;
    float defaultReal = real;
    float defaultImag = imag;

    while (iterations < maximumIteration)
    {
        float tmp_real = real;
        real = (real * real - imag * imag) + defaultReal;
        imag = (2.0 * tmp_real * imag) + defaultImag;

        float dist = real * real + imag * imag;

        if (dist > 4.0)
            break;

        ++iterations;
    }
    return iterations;
}