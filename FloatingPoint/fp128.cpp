#include "fp128.h"

#include <bitset>
#include <iostream>

FP128::FP128()
	: _sign(false)
{
}

FP128::FP128(const int64_t val)
{
	*this = val;
}

FP128::FP128(const float val)
{
	*this = val;
}

FP128::FP128(double val)
{
	*this = val;
}

FP128::FP128(const FP128& that)
{
	_sign = that._sign;
	_exponent = that._exponent;
	_significand = that._significand;
}

bool FP128::isNaN()
{
	if (_exponent.all() && _significand.any())
		return true;
	return false;
}

bool FP128::isInf()
{
	if (_exponent.all() && _significand.none())
		return true;
	return false;
}

bool FP128::isNull()
{
	if (_exponent.none() && _significand.none())
		return true;
	return false;
}

FP128& FP128::operator=(int64_t value)
{
	std::bitset<64> bs(value);
	if (bs.none())
	{
		_sign = 0;
		_exponent = 0;
		_significand = 0;
		return *this;
	}
	if ((bs << 1).none())
	{
		_sign = 1;
		_exponent = 63;
		_exponent[14] = 1;
		_significand = 0;
		return *this;
	}
	_sign = bs[63];
	const bool searchFor = _sign ? 0 : 1;
	uint64_t lastSearchedIndex = 0;
	for (int i = 0; i < 63; ++i)
	{
		if (bs[i] == searchFor)
			lastSearchedIndex = i;
	}
	if (lastSearchedIndex == 0)
	{
		_exponent.set();
		_exponent[14] = 0;
	}
	else
	{
		_exponent = lastSearchedIndex;
		_exponent[14] = 1;
	}
	for (int i = lastSearchedIndex; i > 0; --i)
	{
		if (bs[i])
			_significand[lastSearchedIndex - i] = true;
	}

	return *this;
}

int64_t FP128::toInt64()
{
	std::bitset<64> val;
	if (_sign && _significand.none() && exponentNormalizedValue() == 64)
	{
		return std::numeric_limits<int64_t>::min();
	}
	if (!_sign && _exponent.none() && _significand.none())
	{
		return uint64_t(0);
	}
	_sign ? val.set() : val.reset();
	uint32_t exp = exponentNormalizedValue();
	val[exp - 1] = 1;
	for (int i = exp -2; i >= 0; --i)
	{
		val[i] = _significand[exp - 2 - i];
	}
	return static_cast<int64_t>(val.to_ullong());
}

FP128& FP128::operator=(float value)
{
	std::bitset<32> bs(*reinterpret_cast<uint32_t*>(&value));
	std::bitset<8> floatExp;
	for (int i = 23; i < 31; ++i)
		floatExp[i - 23] = bs[i];
	std::bitset<23> floatSignificand;
	for (int i = 0; i < 23; ++i)
		floatSignificand[i] = bs[i];

	fromBitsets(bs[31], floatExp, floatSignificand);

	return *this;
}

float FP128::toFloat()
{
	return toFP<float, 8, 23>();
}

FP128& FP128::operator=(double value)
{
	std::bitset<64> bs(*reinterpret_cast<uint64_t*>(&value));
	std::bitset<11> doubleExp;
	for (int i = 52; i < 63; ++i)
		doubleExp[i - 52] = bs[i];
	std::bitset<52> doubleSignificand;
	for (int i = 0; i < 52; ++i)
		doubleSignificand[i] = bs[i];

	fromBitsets(bs[63], doubleExp, doubleSignificand);

	return *this;
}

double FP128::toDouble()
{
	return toFP<double, 11, 52>();
}

FP128 FP128::operator+(const FP128& that)
{
	if (_sign != that._sign)
	{
		FP128 lhs = absolute(*this);
		FP128 rhs = absolute(that);
		bool sign;
		if ((_sign && lhs < rhs) || (that._sign && rhs < lhs))
		{
			sign = 0;
		}
		else if (_sign && rhs < lhs || (that._sign && lhs < rhs))
		{
			sign = 1;
		}
		if (lhs == rhs)
			return FP128();
		if (lhs < rhs)
		{
			FP128 value = lhs - rhs;
			value._sign = sign;
			return value;
		}
		if (rhs < lhs)
		{
			FP128 value = lhs - rhs;
			value._sign = sign;
			return value;
		}
	}

	FP128 addTo = that < *this ? *this : that;
	FP128 whatToAdd = *this == addTo ? that : *this;
	bool shiftedBit2 = 0, shiftedBit1 = 0, hiddenBit = 1;
	toSameExponent(addTo, whatToAdd, hiddenBit, shiftedBit1, shiftedBit2);

	bool carryBit = 0;
	for (int i = 0; i < 112; ++i)
	{
		if (whatToAdd._significand[i] == 0 && addTo._significand[i] == 0)
		{
			if (carryBit)
			{
				addTo._significand[i] = 1;
				carryBit = 0;
			}
		}
		else if (whatToAdd._significand[i] == 0 && addTo._significand[i] == 1)
		{
			if (carryBit)
			{
				addTo._significand[i] = 0;
			}
		}
		else if (whatToAdd._significand[i] == 1 && addTo._significand[i] == 0)
		{
			if (!carryBit)
			{
				addTo._significand[i] = 1;
			}
		}
		else if (whatToAdd._significand[i] == 1 && addTo._significand[i] == 1)
		{
			if (!carryBit)
			{
				addTo._significand[i] = 0;
				carryBit = 1;
			}
		}
	}

	if ((hiddenBit && !carryBit) || (!hiddenBit && carryBit))
	{
		shiftedBit2 = shiftedBit1;
		shiftedBit1 = whatToAdd._significand[0];
		addTo._significand >>= 1;
		addTo.setNormalizedExponent(addTo.exponentNormalizedValue() + 1);
	}

	if (carryBit && hiddenBit)
	{
		shiftedBit2 = shiftedBit1;
		shiftedBit1 = whatToAdd._significand[0];
		addTo._significand >>= 1;
		addTo._significand[111] = 1;
		addTo.setNormalizedExponent(addTo.exponentNormalizedValue() + 1);
	}

	roundFP128(addTo, shiftedBit1, shiftedBit2);

	return addTo;
}

FP128 FP128::operator-(const FP128& that)
{
	// Negative minus positive => -1 * abs(this + that)
	// Positive minus negative => abs(this + that)
	if (_sign != that._sign)
	{
		if (!_sign)
			return *this + negated(that);
		else
			return negated(negated(*this) + that);
	}
	
	// Negative minus negative and lhs is lower => -1* abs(that - this) <= Positive minus positive and lhs is lower
	// Negative minus negativ and rhs is lower => -1* abs(this - that) <= positive minus positive and rhs is lower

	if (absolute(*this) == absolute(that))
		return FP128();

	// In all case sub the lower from the greater

	FP128 subFrom = *this <= that ? that : *this;
	FP128 whatToSub = *this <= that ? *this : that;
	bool shiftedBit2 = 0, shiftedBit1 = 0, hiddenBit = 1;
	toSameExponent(subFrom, whatToSub, hiddenBit, shiftedBit1, shiftedBit2);

	bool carryBit = 0;
	for (int i = 0; i < 112; ++i)
	{
		if (whatToSub._significand[i] == 0 && subFrom._significand[i] == 0)
		{
			if (carryBit)
				subFrom._significand[i] = 1;
		}
		else if (whatToSub._significand[i] == 0 && subFrom._significand[i] == 1)
		{
			if (carryBit)
			{
				subFrom._significand[i] = 0;
				carryBit = 0;
			}
		}
		else if (whatToSub._significand[i] == 1 && subFrom._significand[i] == 0)
		{
			if (!carryBit)
			{
				subFrom._significand[i] = 1;
				carryBit = 1;
			}
		}
		else if (whatToSub._significand[i] == 1 && subFrom._significand[i] == 1)
		{
			if (!carryBit)
			{
				subFrom._significand[i] = 0;
			}
		}
	}

	if ((carryBit && !hiddenBit) || (!carryBit && hiddenBit))
	{
		// In this case, the implicit bit would be zero
		int i = 1;
		while (i <= 112 && subFrom._significand[111] == 0)
		{
			subFrom._significand <<= 1;
			auto g = subFrom.toDouble();
			subFrom._significand[0] = shiftedBit1;
			shiftedBit1 = shiftedBit2;
			shiftedBit2 = 0;
			++i;
		}
		subFrom._significand <<= 1;
		subFrom._significand[0] = shiftedBit1;
		shiftedBit1 = shiftedBit2;
		shiftedBit2 = 0;
		subFrom.setNormalizedExponent(subFrom.exponentNormalizedValue() - i);
	}

	roundFP128(subFrom, shiftedBit1, shiftedBit2);

	subFrom._sign = *this < that ? !this->_sign : this->_sign;

	return subFrom;
}

bool FP128::operator<(const FP128& that) const
{
	if (_sign > that._sign)
		return true;
	if (_sign < that._sign)
		return false;
	if (exponentNormalizedValue() < that.exponentNormalizedValue())
		return true;
	if (that.exponentNormalizedValue() < exponentNormalizedValue())
		return false;
	for (int i = 111; i >= 0; --i)
	{
		if (_significand[i] < that._significand[i])
			return true;
		else if (_significand[i] > that._significand[i])
			return false;
	}
	return false;
}

bool FP128::operator<=(const FP128& that) const
{
	return *this < that || !(that < *this);
}

bool FP128::operator==(const FP128& that) const
{
	if (_sign != that._sign)
		return false;
	if (_exponent != that._exponent)
		return false;
	if (_significand != that._significand)
		return false;
	return true;
}

void FP128::toSameExponent(const FP128& greater, FP128& lower, bool& hiddenBit, bool& shiftBit1, bool& shiftBit2)
{
	int32_t greaterExp = greater.exponentNormalizedValue();
	int32_t lowerExp = lower.exponentNormalizedValue();
	if (lowerExp != greaterExp)
	{
		shiftBit2 = shiftBit1;
		shiftBit1 = lower._significand[0];
		lower._significand >>= 1;
		lower._significand[111] = hiddenBit;
		hiddenBit = 0;
		lowerExp++;
	}
	while (lowerExp != greaterExp)
	{
		shiftBit2 = shiftBit1;
		shiftBit1 = lower._significand[0];
		lower._significand >>= 1;
		lowerExp++;
	}
}

int32_t FP128::exponentNormalizedValue() const
{
	return static_cast<uint32_t>(_exponent.to_ullong()) - (((uint32_t)1 << 14) - 1);
}

void FP128::setNormalizedExponent(int32_t value)
{
	const uint32_t center = ((uint32_t)1 << 14) - 1;
	_exponent = center + value;
}

void FP128::roundFP128(FP128& num, bool shiftBit1, bool shiftBit2)
{
	if (shiftBit1 && shiftBit2)
	{
		int i = 0;
		while (num._significand[i] == 1 && i < 112)
		{
			num._significand[i] = 0;
			++i;
		}
		if (i == 112)
		{
			if (!num.isInf())
			{
				while (i < 128 && num._significand[i] == 1)
				{
					num._significand[i] = 0;
					++i;
				}
			}
		}
		else
		{
			num._significand[i] = 1;
		}
	}
	else if (shiftBit1 && shiftBit2)
	{
		// Tie - Round to even
		num._significand[0] = 0;
	}
}

FP128 FP128::negated(const FP128& num)
{
	FP128 negated(num);
	negated._sign = !negated._sign;
	return negated;
}

FP128 FP128::absolute(const FP128& num)
{
	FP128 absolute(num);
	absolute._sign = 0;
	return absolute;
}
