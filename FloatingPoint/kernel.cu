#include "wrapper.hpp"

#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>

template <typename T>
__global__ void cudaMandelbrot(RGB* data, int width, int height, T zoomIn, T centerX, T centerY)
{
	int x = blockIdx.x;
	int y = threadIdx.x;

    T real = ((x - (width / 2) + (centerX / zoomIn * screenWidth)) * zoomIn / (T)width) * 4;
    T imag = ((y - (height / 2) + (centerY / zoomIn * screenHeight)) * zoomIn / (T)height) * 4;

    int iterations = 0;
    T defaultReal = real;
    T defaultImag = imag;

    while (iterations < maximumIteration)
    {
        T tmp_real = real;
        real = (real * real - imag * imag) + defaultReal;
        imag = (2.0 * tmp_real * imag) + defaultImag;

        T dist = real * real + imag * imag;

        if (dist > 4.0)
            break;

        ++iterations;
    }

    RGB& dt = data[y * height + x];
    dt.r = 0;
    dt.b = 0;

    if (iterations == maximumIteration)
    {
        dt.g = 0;
        return;
    }
    const float div = (float)iterations / maximumIteration;
    const float value = (float)255 * div;
    dt.g = value;
}

void cudaCallMandelbrotFloat(RGB* data, int width, int height, float zoomIn, float centerX, float centerY)
{
	size_t dataSize = width * height * sizeof(RGB);
	RGB* deviceData;
	cudaMalloc(&deviceData, dataSize);

	// Thread / Block calculation

	dim3 blocks(width);
	dim3 threads(height);

	cudaMandelbrot<float><<<blocks, threads>>>(deviceData, width, height, zoomIn, centerX, centerY);

	cudaMemcpy(data, deviceData, dataSize, cudaMemcpyDeviceToHost);

    cudaFree(deviceData);
}

void cudaCallMandelbrotDouble(RGB* data, int width, int height, float zoomIn, float centerX, float centerY)
{
    size_t dataSize = width * height * sizeof(RGB);
    RGB* deviceData;
    cudaMalloc(&deviceData, dataSize);

    // Thread / Block calculation

    dim3 blocks(width);
    dim3 threads(height);

    cudaMandelbrot<double> << <blocks, threads >> > (deviceData, width, height, static_cast<double>(zoomIn), static_cast<double>(centerX), static_cast<double>(centerY));

    cudaMemcpy(data, deviceData, dataSize, cudaMemcpyDeviceToHost);

    cudaFree(deviceData);
}