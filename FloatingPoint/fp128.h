#pragma once

#include <bitset>
#include <cinttypes>

class FP128
{
public:
	FP128();
	FP128(int64_t val);
	FP128(float val);
	FP128(double val);
	FP128(const FP128& that);
	~FP128() = default;

	bool isNaN();
	bool isInf();
	bool isNull();

	FP128& operator=(int64_t value);
	int64_t toInt64();

	FP128& operator=(float value);
	float toFloat();

	FP128& operator=(double value);
	double toDouble();

	FP128 operator+(const FP128& that);
	FP128 operator-(const FP128& that);

	bool operator<(const FP128& that) const;
	bool operator<=(const FP128& that) const;
	bool operator==(const FP128& that) const;

	void toSameExponent(const FP128& greater, FP128& lower, bool& hiddenBit, bool& shiftBit1, bool& shiftBit2);

private:
	int32_t exponentNormalizedValue() const;
	void setNormalizedExponent(int32_t value);
	
	template<std::size_t exponentNumber, std::size_t significandNumber>
	void fromBitsets(bool sign, const std::bitset<exponentNumber>& exponent, const std::bitset<significandNumber>& significand);

	template<std::size_t significandNumber>
	void copySignificand(const std::bitset<significandNumber>& significand);

	template<class T, std::size_t exponentNumber, std::size_t significandNumber>
	T& toFP();

	void roundFP128(FP128& num, bool shiftBit1, bool shiftBit2);
	FP128 negated(const FP128& num);
	FP128 absolute(const FP128& num);

private:
	bool _sign;
	std::bitset<15> _exponent;
	std::bitset<112> _significand;
};

template<std::size_t exponentNumber, std::size_t significandNumber>
inline void FP128::fromBitsets(bool sign, const std::bitset<exponentNumber>& exponent, const std::bitset<significandNumber>& significand)
{
	_sign = sign;
	// NaN
	if (exponent.all() && significand.any())
	{
		_exponent.set();
		_significand.reset();
		copySignificand(significand);
		return;
	}
	// Inf
	if (exponent.all() && significand.none())
	{
		_exponent.set();
		_significand.reset();
		return;
	}

	const int exponentValue = static_cast<int32_t>(exponent.to_ullong()) - ((1 << (exponentNumber - 1)) - 1);

	// Subnormal
	if (exponent.none() && significand.any())
	{
		setNormalizedExponent(exponentValue);
		copySignificand(significand);
		return;
	}

	// Zero
	if (exponent.none() && significand.none())
	{
		_exponent.reset();
		_significand.reset();
		return;
	}

	// Normal fp
	setNormalizedExponent(exponentValue);
	copySignificand(significand);
}

template<std::size_t significandNumber>
inline void FP128::copySignificand(const std::bitset<significandNumber>& significand)
{
	for (int i = 0; i < significandNumber; ++i)
	{
		_significand[111 - i] = significand[significandNumber - i - 1];
	}
}

template<class T, std::size_t exponentNumber, std::size_t significandNumber>
inline T& FP128::toFP()
{
	const size_t bitNumber = sizeof(T) * 8;
	std::bitset<bitNumber> value;
	value.reset();
	value[bitNumber-1] = _sign;
	const int32_t maxExponent = (1 << exponentNumber - 1) - 1;
	const int32_t minExponent = 1 - maxExponent;
	// NaN
	if (isNaN())
	{
		for (int i = significandNumber; i < bitNumber-1; ++i)
			value[i] = 1;
		value[0] = 1; // It's NaN by ieee754
		return *reinterpret_cast<T*>(&value);
	}
	// Inf
	if (exponentNormalizedValue() > maxExponent)
	{
		for (int i = significandNumber; i < bitNumber-1; ++i)
			value[i] = 1;
		return *reinterpret_cast<T*>(&value);
	}
	// Zero
	if (isNull())
	{
		for (int i = 0; i < bitNumber - 1; ++i)
			value[i] = 0;
		return *reinterpret_cast<T*>(&value);
	}
	// Subnormal
	if (exponentNormalizedValue() < minExponent)
	{
		for (int i = significandNumber; i < bitNumber - 1; ++i)
			value[i] = 0;
		bool hasTrue = 0;
		for (int i = 0; i < significandNumber; ++i)
		{
			value[significandNumber - i - 1] = _significand[111 - i];
			if (_significand[111 - i])
				hasTrue = true;
		}
		if (!hasTrue)
			value[0] = 1; // As this has to be subnormal
		return *reinterpret_cast<T*>(&value);
	}

	//If normal fp
	for (int i = significandNumber; i < bitNumber - 2; ++i)
		value[i] = _exponent[i - significandNumber];
	value[bitNumber-2] = _exponent[14];

	for (int i = 0; i < significandNumber; ++i)
		value[significandNumber - 1 - i] = _significand[111 - i];

	// Rounding - Round to nearest
	if (_significand[111 - significandNumber] && _significand[111 - significandNumber - 1])
	{
		int i = 0;
		while (value[i] == 1 && i < significandNumber)
		{
			value[i] = 0;
			++i;
		}
		if (i == significandNumber)
		{
			bool isInfAlready = true;
			int j = i;
			while (j < bitNumber - 1 && isInfAlready)
			{
				if (value[i] == 0)
					isInfAlready = false;
				++j;
			}
			if (!isInfAlready)
			{
				while (i < bitNumber - 1 && value[i] == 1)
				{
					value[i] = 0;
					++i;
				}
				value[i] = 1;
			}
		}
		else
		{
			value[i] = 1;
		}
	}
	else if (_significand[111 - significandNumber] && !_significand[111 - significandNumber - 1])
	{
		// Tie - Round to even
		value[0] = 0;
	}

	return *reinterpret_cast<T*>(&value);
}
