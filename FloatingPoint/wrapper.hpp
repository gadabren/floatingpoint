#ifndef WRAPPER
#define WRAPPER

#include <cinttypes>

#include "fp128.h"

constexpr int screenWidth = 768;
constexpr int screenHeight = 768;
constexpr int maximumIteration = 500;

struct RGB
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

void cudaCallMandelbrotFloat(RGB* data, int width, int height, float zoomIn, float centerX, float centerY);
void cudaCallMandelbrotDouble(RGB* data, int width, int height, float zoomIn, float centerX, float centerY);

#endif